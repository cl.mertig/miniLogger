"""
This is an example 'config.py' file.
It should be reachable by the logger.py.
It solely consists of global variables.
It should not be used for any other type of variables or code.
"""

# -------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------LOGGER-CONFIG---------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------

# ANSI color encodings needed to color the console logs of the log()-function:
_MESSAGE_CONSOLE_COLOR = "\033[1;34m"       # blue
_WARNING_CONSOLE_COLOR = "\033[1;32m"    # orange
_ERROR_CONSOLE_COLOR = "\033[1;31m"      # red
_DEFAULT_CONSOLE_COLOR = "\033[0;0m"

# where to log to
_CONSOLE_LOG = True
_FILE_LOG = True

# what the log file should be named like (needed if FILE_LOG is true)
_LOGFILE_NAME = "log.txt"

# -------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------END-OF-CONFIG---------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------
