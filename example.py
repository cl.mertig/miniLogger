from mini_logger import log

log(__name__, "message", "This is a simple message. Nothing weird happened.")
log(__name__, "warning", "This is a warning. You should check this out!")
log(__name__, "error", "This is an error! Immediate action might be required!")
