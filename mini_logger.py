"""
This is the 'logger.py' file.
It is a default import for every other file of the project.
It mainly consists of the log()-function meant to store logging-data in a database
as well as printing it to the console.
"""

# -------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------DEFAULT-CONFIG---------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------

# ANSI color encodings needed to color the console logs of the log()-function:
_MESSAGE_CONSOLE_COLOR = "\033[1;34m"       # blue
_WARNING_CONSOLE_COLOR = "\033[1;32m"    # orange
_ERROR_CONSOLE_COLOR = "\033[1;31m"      # red
_DEFAULT_CONSOLE_COLOR = "\033[0;0m"

# where to log to
_CONSOLE_LOG = True
_FILE_LOG = True

# what the log file should be named like (needed if FILE_LOG is true)
_LOGFILE_NAME = "log.txt"

# -------------------------------------------------------------------------------------------------------------------
# ---------------------------------------------------END-OF-CONFIG---------------------------------------------------
# -------------------------------------------------------------------------------------------------------------------


def log(origin: str, message_type: str, message: str):
    """
    This is the only function of the module. You don't need anything more.
    If you want to configure coloring, the name of the logfile or the type of logging, check out the logger its self.
    log() is using the datetime library to add the current datetime using the get_datetime() which is to be found here.
    in "%d.%m.%Y %T %f"-format to the string stored and printed.
    :param origin:
    should be the same as the filename you are calling this function from,
    len() of it should be a maximum of 20 if you want the output to look good with the default monospace font
    :type origin: str
    :param message_type: must be either 'message' or 'warning' or 'error' and will determine the printed strings color
    :type message_type: str
    :param message: the string you actually want to log
    :type message: str
    :raises TypeError: in case one of the arguments is not a string
    :raises ValueError: in case the second argument is not 'message', 'warning' or 'error'
    :rtype NoneType:
    :return None:
    """
    try:
        # checking types
        if not (isinstance(origin, str) and isinstance(message_type, str) and isinstance(message, str)):
            raise TypeError

        # get time and date as specified in http/1.0 protocol rfc1945: https://tools.ietf.org/pdf/rfc1945.pdf#30.
        # Example: Wed, 22 Apr 2020 14:11:55 GMT
        time_date = dt.now(timezone.utc).strftime("%a, %d %b %Y %X GMT")

        # generate the log string we use to log to console or file
        log_string = f"{time_date} | {_center(message_type, 8)} | {_center(origin, 25)} | {message}"

        if _CONSOLE_LOG:
            # set an appropriate color
            if message_type == "message":
                stdout.write(_MESSAGE_CONSOLE_COLOR)
            elif message_type == "warning":
                stdout.write(_WARNING_CONSOLE_COLOR)
            elif message_type == "error":
                stdout.write(_ERROR_CONSOLE_COLOR)
            else:
                raise ValueError

            print(log_string)
            # Reset the color in case a different module wants to write something to the console as well.
            stdout.write(_DEFAULT_CONSOLE_COLOR)

        # write log to .txt file
        if _FILE_LOG:
            with open(_LOGFILE_NAME, 'at') as file:
                file.write(log_string + '\n')

    except ValueError:
        log(__name__, "warning", "'message_type' of 'logger.log()' can only be 'message', 'warning', or 'error'!")

    except TypeError:
        log(__name__, "warning", "'logger.log()' can not log non-strings!")

    except Exception as EE:
        # simply printing this one, because apparently there seems to be something wrong with the logger its self!
        print(f"Caught unhandled error:\n{str(EE)}")


def _center(to_center: str, result_len: int):
    additional_spaces = ' ' * int((result_len - len(to_center)) / 2)
    return f"{additional_spaces}{to_center}{additional_spaces}"


if __name__ == "__main__":
    exit(0)
else:
    try:
        from sys import stdout
        from datetime import datetime as dt, timezone
        from importlib import util
        if util.find_spec("config"):
            from config import *
    except ImportError as IE:
        print(f"Looks like a required dependency cant be imported:\n{str(IE)}")
