# MiniLogger

A very small logger file to plug into any project for easy logging.

MiniLogger wants to be reliable as well as fully documented and easy to use.

## Installation
Copy the *logger.py* to your src folder.

If you want custom configuration, copy the *config.py* to your src folder and then change said config file to your liking.

## Usage
To log something, call log(origin: str, info_type: str, info: str):
- origin : This should always be "\_\_name__"
- info_type : This must be 'info', 'warning' or 'error'.
- info : This can be information you want to log.

If you need an example, please refer to *example.py*.

## Dependencies
The *logger.py* file imports the following modules:
- sys
- datetime
- importlib

All of those modules should be preinstalled with your python3 environment anyway.
So usually no separate installation is required.

## Configuration

### Logging Types
CONSOLE_LOG should be set to True if you want to log to the console.

FILE_LOG should be set to True if you want to log to a file.

LOGFILE_NAME must be set to a string if FILE_LOG is set to True.
LOGFILE_NAME should end with a ".txt".


### ANSI color encodings
If you want to configure the logging visually, please refer to the table below. 
Simply exchange the encoding you picked with the existing ones in
- INFO_CONSOLE_COLOR
- WARNING_CONSOLE_COLOR
- ERROR_CONSOLE_COLOR

or
- DEFAULT_CONSOLE_COLOR
to change said formatting.

| #      	| Regular Colors 	| Bold           	| Underline      	| Background   	| High Intensity 	| Bold High Intensity 	| High Intensity backgrounds 	|
|--------	|----------------	|----------------	|----------------	|--------------	|----------------	|---------------------	|----------------------------	|
| Black  	| \[\033[0;30m\] 	| \[\033[1;30m\] 	| \[\033[4;30m\] 	| \[\033[40m\] 	| \[\033[0;90m\] 	| \[\033[1;90m\]      	| \[\033[0;100m\]            	|
| Red    	| \[\033[0;31m\] 	| \[\033[1;31m\] 	| \[\033[4;31m\] 	| \[\033[41m\] 	| \[\033[0;91m\] 	| \[\033[1;91m\]      	| \[\033[0;101m\]            	|
| Green  	| \[\033[0;32m\] 	| \[\033[1;32m\] 	| \[\033[4;32m\] 	| \[\033[42m\] 	| \[\033[0;92m\] 	| \[\033[1;92m\]      	| \[\033[0;102m\]            	|
| Yellow 	| \[\033[0;33m\] 	| \[\033[1;33m\] 	| \[\033[4;33m\] 	| \[\033[43m\] 	| \[\033[0;93m\] 	| \[\033[1;93m\]      	| \[\033[0;103m\]            	|
| Blue   	| \[\033[0;34m\] 	| \[\033[1;34m\] 	| \[\033[4;34m\] 	| \[\033[44m\] 	| \[\033[0;94m\] 	| \[\033[1;94m\]      	| \[\033[0;104m\]            	|
| Purple 	| \[\033[0;35m\] 	| \[\033[1;35m\] 	| \[\033[4;35m\] 	| \[\033[45m\] 	| \[\033[0;95m\] 	| \[\033[1;95m\]      	| \[\033[0;105m\]            	|
| Cyan   	| \[\033[0;36m\] 	| \[\033[1;36m\] 	| \[\033[4;36m\] 	| \[\033[46m\] 	| \[\033[0;96m\] 	| \[\033[1;96m\]      	| \[\033[0;106m\]            	|
| White  	| \[\033[0;37m\] 	| \[\033[1;37m\] 	| \[\033[4;37m\] 	| \[\033[47m\] 	| \[\033[0;97m\] 	| \[\033[1;97m\]      	| \[\033[0;107m\]            	|

## Support
If you want any additional features found a bug or have an idea, just open an issue!

## Roadmap
### Might be introduced at some point:
- Unittests
- Custom datetime configuration options
- Enable writing files to a specific folder
- Enable logging to an SQLight database

## Contributing
This is a very small project, but if you want you can contribute. Just fork and do a pull request. 

## Authors and acknowledgment
So far I did this myself. *pats own back*

## License
Copyright (c) 2021 Cedric Mertig

For more information please refer to [choosealicense.com](https://choosealicense.com/licenses/mit/#).

## Project status
This project is currently maintained.
